package com.nexsoft;

public class Card {
    Integer[] card = {1, 2, 3, 4, 5};
    String[] name = {"Asus", "Budi", "Ibud", "Dell", "Sosro"};
    String[] jabatan = {"Karyawan", "Manager", "Karyawan", "Karyawan", "Manager"};

    public Integer[] getCard() {
        return card;
    }

    public String[] getName() {
        return name;
    }

    public String[] getJabatan() {
        return jabatan;
    }
}